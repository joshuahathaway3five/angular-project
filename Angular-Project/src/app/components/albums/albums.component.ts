import { Component, OnInit } from '@angular/core';
import {Albums} from "../../Model/Albums";


@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  album: Albums[]

  constructor() {}

  ngOnInit(): void {

    this.album = [{
      title: "Miracle",
      artist: "Blackmill",
      songs: ["Spirit Of Life, Miracle, Let It Be"],
      favorite: "Spirit Of Life",
      year: 2011,
      genre: "Electronic",
      units: 0,
      cover: "",
      active: true
  },
      {
        title: "Furi Original Soundtrack",
        artist: "Scattle",
        songs: ["Wisdom Of Rage, What We Fight For, You're Mine"],
        favorite: "You're Mine",
        year: 2016,
        genre: "Electronic",
        units: 0,
        cover: "",
        active: false
      },
      {
        title: "Gliss",
        artist: "Tomas Skyldeberg",
        songs: ["Curiosity, Dual Minds, Soul Crazy"],
        favorite: "Curiosity",
        year: 2016,
        genre: "Electronic",
        units: 0,
        cover: "../../assets/Image/Miracle.jpg",
        active: true
      },
      {
        title: "Fake Album",
        artist: "Unknown",
        songs: ["Stuff, More Stuff, Even More Stuff"],
        favorite: "Even More Stuff",
        year: 2020,
        genre: "Death Metal",
        units: 0,
        cover: "",
        active: false
      },
      {
        title: "Fake Album2",
        artist: "Unknown2",
        songs: ["Nothing, More Nothing, Even MORE Nothing"],
        favorite: "Nothing",
        year: 2021,
        genre: "Silent",
        units: 0,
        cover: "",
        active: true
      }]

}

  addAlbum(NewAlbum: Albums) {
    this.album.push(NewAlbum)
  }

  //
  // AlbumColor(x){
  //   for(let i=0; i<this.album.length; i++){
  //     if(i % 2 === 0){
  //       this.album[i].active = true;{
  //       }
  //     } else {
  //       this.album[i].active = false;
  //     }
  //   }
  //   console.log(this.album);
  // }








}
