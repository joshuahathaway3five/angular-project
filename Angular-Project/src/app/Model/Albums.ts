export interface Albums {
  active: any;
  title: string,
  artist: string,
  songs: string[],
  favorite: string,
  year: number,
  genre: string,
  units: number,
  cover?: any
  // img?: string



}
